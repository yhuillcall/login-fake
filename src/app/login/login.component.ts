import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent{
  router;
  checkoutForm;
  constructor(
    private formBuilder: FormBuilder,router: Router,route:ActivatedRoute
    ) {
      this.router = router
      route.params.subscribe(val => {
        const isLogged = localStorage.getItem('isLogged') == "1" ? true : false;
        console.log(isLogged);
        if  (isLogged) {
          console.log("Redirect to home");
          // this.router.navigate(['login']);
          this.router.navigateByUrl("/home", { skipLocationChange: false });
        }
      }); 
    this.checkoutForm = this.formBuilder.group({
    username: '',
    password: ''
  });
}


  onSubmit(customerData) {
    localStorage.setItem('isLogged','1');
    if ( customerData.username != "admin" && customerData.password != "123456"){
      alert("Datos incorrectos");
    }else{
      this.router.navigateByUrl("/home", { skipLocationChange: false });
    }
    
  }
}
