import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  title = 'Bienvenido';
  name = 'Yessica Milagroxxx';
  constructor(private router: Router,route:ActivatedRoute) {
    route.params.subscribe(val => {
      const isLogged = localStorage.getItem('isLogged') == "1" ? true : false;
      console.log(isLogged);
      if  (!isLogged) {
        console.log("Redirect to login");
        // this.router.navigate(['login']);
        this.router.navigateByUrl("/login", { skipLocationChange: false });
      }
    });
   }

  ngOnInit(): void {
  }

  onSubmit() {
    localStorage.setItem('isLogged','0');
    console.log("Redirect to login");
    this.router.navigateByUrl("/login", { skipLocationChange: false });
  }
}
